package com.qa.rest.test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.net.URI;
import java.net.URISyntaxException;

import static io.restassured.RestAssured.*;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGet {

	@Test
	public void getRequestTest() throws URISyntaxException{
		
		/**
		 * Given Accept response in Json format
		 * when i perform the get request
		 */
		
		URI uri=new URI("http://ergast.com/api/f1/2017/circuits");
		
		//RestAssured.when().get(uri);
		Response response=given()
		.accept(ContentType.JSON)
		.when()
		.get(uri);
		//Response response =when().get(uri);
		System.out.println(response.asString());
	}
	
	@Test
	public void testStatusCode() throws URISyntaxException{
		/**
		 * Given accept response json Format
		 * when i perform the get request
		 * then Status Code 200 ok should return 
		 */
		
		/*URI uri=new URI("http://ergast.com/api/f1/2017/circuits");
		int statusCode=given()
		.accept(ContentType.JSON)
		.when()
		.get(uri)
		.thenReturn()
		.statusCode();
		System.out.println("Response Code : "+statusCode);
		Assert.assertEquals(HttpStatus.SC_OK, statusCode);*/
		
		URI uri= new URI("http://ergast.com/api/f1/2017/circuits");
		given()
		.accept(ContentType.JSON)
		.when()
		.get(uri)
		.then()
		.assertThat()
		.statusCode(HttpStatus.SC_OK);
	}
	@Test
	public void testGetDriver(){
		/**
		 * Given accept response json format
		 * when i perform the get request
		 * then status code 200 ok should return
		 * 	
		 */
		given()
		.accept(ContentType.JSON)
		.when()
		.get("limit=10&offset=20");
		
	}
	
}
